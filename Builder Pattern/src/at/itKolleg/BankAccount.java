package at.itKolleg;
public class BankAccount {

    private long accountNumber;
    private String owner;
    private String branch;
    private double balance;
    private double interestRate;

//    public BankAccount(long accountNumber, String owner, String branch, double balance, double interestRate) {
//        this.accountNumber = accountNumber;
//        this.owner = owner;
//        this.branch = branch;
//        this.balance = balance;
//        this.interestRate = interestRate;
//    }


    public static class Builder {

        private long accountNumber; //Dies ist wichtig, also übergeben wir es an den Konstruktor.
        private String owner;
        private String branch;
        private double balance;
        private double interestRate;

//        public Builder (){
//
//        }

        public Builder(long accountNumber) {
            this.accountNumber = accountNumber;
        }

        public Builder withOwner(String owner){
            this.owner = owner;

            return this;  //Indem wir jedes Mal den Builder zurückgeben, können wir ein fließendes Interface schaffen.
        }

        public Builder atBranch(String branch){
            this.branch = branch;

            return this;
        }

        public Builder openingBalance(double balance){
            this.balance = balance;

            return this;
        }

        public Builder atRate(double interestRate){
            this.interestRate = interestRate;

            return this;
        }

        public BankAccount build(){
            //Hier erstellen wir das eigentliche Bankkonto-Objekt, das sich immer in einem vollständig initialisierten Zustand befindet, wenn es zurückgegeben wird.
            BankAccount account = new BankAccount();  //Da sich der Builder in der Klasse BankAccount befindet, können wir seinen privaten Konstruktor aufrufen.
            account.accountNumber = this.accountNumber;
            account.owner = this.owner;
            account.branch = this.branch;
            account.balance = this.balance;
            account.interestRate = this.interestRate;

            return account;
        }
    }

    @Override
    public String toString() {
        return "Builder{" +
                "accountNumber=" + accountNumber +
                ", owner='" + owner + '\'' +
                ", branch='" + branch + '\'' +
                ", balance=" + balance +
                ", interestRate=" + interestRate +
                '}';
    }


    private BankAccount() {
        //Konstruktor ist jetzt private.
    }



}