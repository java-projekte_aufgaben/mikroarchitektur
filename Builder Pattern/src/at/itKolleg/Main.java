package at.itKolleg;

public class Main {

    public static void main(String[] args) {

//      BankAccount account = new BankAccount.Builder()
//                .withOwner("Bart")
//                .atBranch("Springfield")
//                .openingBalance(200)
//                .atRate(1.5)
//                .build();

      BankAccount account = new BankAccount.Builder(1234L)
                .withOwner("Marge")
                .atBranch("Springfield")
                .openingBalance(100)
                .atRate(2.5)
                .build();


      BankAccount anotherAccount = new BankAccount.Builder(4567L)
                .withOwner("Homer")
                .atBranch("Springfield")
                .openingBalance(100)
                .atRate(2.5)
                .build();

        System.out.println(account.toString());
        System.out.println("################################################################");
        System.out.printf(anotherAccount.toString());
    }
}
