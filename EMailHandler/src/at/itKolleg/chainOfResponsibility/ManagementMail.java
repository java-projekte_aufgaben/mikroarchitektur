package at.itKolleg.chainOfResponsibility;

import at.itKolleg.exceptions.KeineZustaendigkeit;

public class ManagementMail extends MailVerteiler{

    public ManagementMail(MailVerteiler naechsterVerteiler) {
        super(naechsterVerteiler);

    }

    @Override
    public String weiterleiten(String betreff) throws KeineZustaendigkeit {

        if(betreff.equals("Management")){
            return "Diese E-Mail ist für die Managementabteilung!\n";
        } else {
            return super.weiterleiten(betreff);
        }
    }
}
