package at.itKolleg.chainOfResponsibility;

import at.itKolleg.exceptions.KeineZustaendigkeit;

public class ServiceMail extends MailVerteiler {

    public ServiceMail(MailVerteiler naechsterVerteiler) {
        super(naechsterVerteiler);
    }

    @Override
    public String weiterleiten(String betreff) throws KeineZustaendigkeit {

        if(betreff.equals("Service")){
            return "Diese E-Mail ist für die Serviceabteilung!\n";

        } else {
            return super.weiterleiten(betreff);
        }

    }
}
