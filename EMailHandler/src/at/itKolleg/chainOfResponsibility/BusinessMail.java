package at.itKolleg.chainOfResponsibility;

import at.itKolleg.exceptions.KeineZustaendigkeit;

public class BusinessMail extends MailVerteiler{

    public BusinessMail(MailVerteiler naechsterVerteiler){

        super(naechsterVerteiler);
    }

    @Override
    public String weiterleiten(String betreff) throws KeineZustaendigkeit {

        if(betreff.equals("Business")){
            return "Diese E-Mail ist für die Buisenessabteilung!\n";
        } else {
            return super.weiterleiten(betreff);
        }
    }


}
