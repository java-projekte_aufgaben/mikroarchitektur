package at.itKolleg.chainOfResponsibility;

import at.itKolleg.Weiterleitung;
import at.itKolleg.exceptions.KeineZustaendigkeit;

public abstract class MailVerteiler implements Weiterleitung {

    private Weiterleitung naechsterVerteiler;

    public MailVerteiler (Weiterleitung naechsterVerteiler){

        this.naechsterVerteiler = naechsterVerteiler;
    }

    @Override
    public String weiterleiten(String betreff) throws KeineZustaendigkeit {

        if(naechsterVerteiler != null){
            return naechsterVerteiler.weiterleiten(betreff);
        } else {
            throw new KeineZustaendigkeit();
        }
    }


}
