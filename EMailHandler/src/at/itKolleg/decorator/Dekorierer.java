package at.itKolleg.decorator;

import at.itKolleg.Weiterleitung;
import at.itKolleg.chainOfResponsibility.MailVerteiler;
import at.itKolleg.exceptions.KeineZustaendigkeit;

public class Dekorierer extends MailVerteiler {

    public Dekorierer(Weiterleitung naechsterDekorierer){
        super(naechsterDekorierer);
    }

    @Override
    public String weiterleiten(String betreff) throws KeineZustaendigkeit {
        return super.weiterleiten(betreff);
    }
}
