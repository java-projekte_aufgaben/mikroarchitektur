package at.itKolleg.decorator;

import at.itKolleg.Weiterleitung;
import at.itKolleg.exceptions.KeineZustaendigkeit;

public class HeaderDekorierer extends Dekorierer{

    public HeaderDekorierer(Weiterleitung naechsterDekorierer) {
        super(naechsterDekorierer);
    }

    @Override
    public String weiterleiten(String betreff) throws KeineZustaendigkeit {

        if(betreff.equals("Business")){
            System.out.println("++++++++++++ BUSINESS MAIL ++++++++++++");
            return super.weiterleiten(betreff);
        } else if (betreff.equals("Service")){
            System.out.println("++++++++++++ SERVICE MAIL ++++++++++++");
            return super.weiterleiten(betreff);
        } else if (betreff.equals("Management")){
            System.out.println("++++++++++++ MANAGEMENT MAIL ++++++++++++");
            return super.weiterleiten(betreff);
        }
        return super.weiterleiten(betreff);
    }
}
