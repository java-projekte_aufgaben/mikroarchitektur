package at.itKolleg;

import at.itKolleg.chainOfResponsibility.BusinessMail;
import at.itKolleg.chainOfResponsibility.ManagementMail;
import at.itKolleg.chainOfResponsibility.ServiceMail;
import at.itKolleg.decorator.HeaderDekorierer;
import at.itKolleg.exceptions.KeineZustaendigkeit;

public class Main {

    public static void main(String[] args) {

        Weiterleitung kette = new BusinessMail(new ServiceMail(new ManagementMail(null)));

        kette = new HeaderDekorierer(kette);

        try {
            System.out.println(kette.weiterleiten("Management"));
            System.out.println(kette.weiterleiten("Service"));
            System.out.println(kette.weiterleiten("Business"));
        } catch (KeineZustaendigkeit e) {
            e.printStackTrace();
        }
    }
}
