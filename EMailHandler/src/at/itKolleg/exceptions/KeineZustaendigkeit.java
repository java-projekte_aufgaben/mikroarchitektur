package at.itKolleg.exceptions;

public class KeineZustaendigkeit extends Exception{

    public String message = "Keine Zuständigkeit gefunden!";

    @Override
    public String getMessage() {
        return message;
    }
}
