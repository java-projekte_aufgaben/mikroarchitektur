package at.itKolleg;

import at.itKolleg.exceptions.KeineZustaendigkeit;

public interface Weiterleitung {

    public String weiterleiten(String betreff) throws KeineZustaendigkeit;
}
