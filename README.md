# Mikroarchitektur

# Inhaltsverzeichnis

- [Mikroarchitektur](#mikroarchitektur)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Entwurfsmuster 1](#entwurfsmuster-1)
  - [Ein Entwurfsmuster](#ein-entwurfsmuster)
  - [Beschreibung von Entwurfsmustern](#beschreibung-von-entwurfsmustern)
  - [Nutzen von Entwurfsmustern](#nutzen-von-entwurfsmustern)
  - [Unterteilung von Entwurfsmustern](#unterteilung-von-entwurfsmustern)
  - [Strategy Pattern](#strategy-pattern)
    - [UML - Strategy Pattern](#uml---strategy-pattern)
- [Entwurfsmuster 2](#entwurfsmuster-2)
  - [Chain of Responsibility](#chain-of-responsibility)
    - [Verwendung](#verwendung)
  - [Template Hook Pattern](#template-hook-pattern)
  - [Decorator Pattern](#decorator-pattern)
    - [Welchen Zweck erfüllt das Decorator Design Pattern?](#welchen-zweck-erfüllt-das-decorator-design-pattern)
    - [Die Vorteile und Nachteile des Decorator Patterns im Überblick](#die-vorteile-und-nachteile-des-decorator-patterns-im-überblick)
  - [Builder Pattern](#builder-pattern)
    - [Das Builder Pattern im Detail](#das-builder-pattern-im-detail)
    - [Vorteile des Builder-Patterns](#vorteile-des-builder-patterns)
    - [Nachteile des Builder Patterns](#nachteile-des-builder-patterns)
    - [Wo kommt das Builder Pattern zum Einsatz?](#wo-kommt-das-builder-pattern-zum-einsatz)
  - [Adapter](#adapter)
    - [Verwendung](#verwendung-1)
    - [UML Diagramm](#uml-diagramm)
      - [Adapter mit Delegation (Objektadapter)](#adapter-mit-delegation-objektadapter)
    - [Adapter mit Vererbung (Klassenadapter)](#adapter-mit-vererbung-klassenadapter)
    - [Vor- und Nachteile](#vor--und-nachteile)
  - [Observer Pattern](#observer-pattern)
    - [Zweck und Funktionsweise des Observer Patterns](#zweck-und-funktionsweise-des-observer-patterns)
    - [Grafische Darstellung des Observer Patterns (UML-Diagramm)](#grafische-darstellung-des-observer-patterns-uml-diagramm)
    - [Welche Vor- und Nachteile hat das Observer Design Pattern?](#welche-vor--und-nachteile-hat-das-observer-design-pattern)
    - [Wo wird es eingesetzt?](#wo-wird-es-eingesetzt)
- [Solid Design Prinzipien](#solid-design-prinzipien)
  - [Single Responsibility Prinzip](#single-responsibility-prinzip)
  - [Open Closed Prinzip](#open-closed-prinzip)
  - [Liskovsches Substitutions Prinzip](#liskovsches-substitutions-prinzip)
  - [Interface Segregation Prinzip](#interface-segregation-prinzip)
  - [Dependency Inversion Prinzip](#dependency-inversion-prinzip)
- [Weitere Prinzipien des objektorientierten Entwurfs](#weitere-prinzipien-des-objektorientierten-entwurfs)
  - [KISS Prinzip](#kiss-prinzip)
  - [DRY Prinzip](#dry-prinzip)
  - [YAGNI Prinzip](#yagni-prinzip)

<br><br>

# Entwurfsmuster 1 

Entwurfsmuster (englisch design patterns) sind bewährte Lösungsschablonen für wiederkehrende Entwurfsprobleme sowohl in der Architektur als auch in der Softwarearchitektur und -entwicklung. Sie stellen damit eine wiederverwendbare Vorlage zur Problemlösung dar, die in einem bestimmten Zusammenhang einsetzbar ist.
<br><br>

## Ein Entwurfsmuster 

- beschreibt abstrakt eine bewährte Lösung für ein bestimmtes und häufig wiederkehrendes Problem des objektorientierten Softwareentwurfs 
- entsteht durch die Analyse und Überarbeitung vorhandener Designlösungen, setzt also Entwurfserfahrung voraus (Entwurfsmuster werden nicht “erfunden”, sondern entdeckt !) 
- kann in seiner Struktur verstanden werden als eine Menge von Klassen, die festgelegte Verantwortlichkeiten haben und in einer definierten Vererbungs- bzw. Benutzungsbeziehungen zueinander stehen 
- kann in seiner Dynamik verstanden werden als eine Menge von Objekten, die nach einem beschreibbaren Prinzip interagieren bzw. erzeugt werden 
- kann immer nur zusammen mit dem Entwurfsproblem beschrieben werden, das es lösen soll
<br><br>

## Beschreibung von Entwurfsmustern

Eine Musterbeschreibung besteht meist aus den folgenden Teilen:

- dem Namen des Entwurfsmusters, 
- dem Problem, das mit Hilfe des Musters gelöst werden soll, 
- der Kontext, in dem sich das Problem stellt, 
- der Lösung, mit der die Organisation von Klassen in einer Klassenhierarchie und die Gestaltung ihrer Interaktion vorgegeben werden, 
- die (positiven und negativen) Konsequenzen der Musteranwendung.
<br><br>

## Nutzen von Entwurfsmustern

- helfen, existierende Softwareentwürfe zu analysieren und zu reorganisieren 
- erleichtern die Einarbeitung in Software-Architekturen (z.B. Klassenbibliotheken, Rahmenwerke), solange sie auf der Basis von bekannten Entwurfsmustern dokumentiert sind 
- sind “Mikroarchitekturen”, die sich von erfahrenen Entwicklern als Bausteine innerhalb größerer Software-Architekturen wiederverwenden lassen (Wiederverwendung von DesignLösungen statt Wiederverwendung von Code) 
- stellen uns die Elemente einer Sprache, in der wir über Software-Architekturen nachdenken und kommunizieren können 
- sollen die softwaretechnische Qualität von Entwürfen erhöhen (z.B. ihre Wiederverwendbarkeit und Erweiterbarkeit)
<br><br>

## Unterteilung von Entwurfsmustern

Der allgemeine Teil eines Entwurfsmusters:

- beschreibt die abstrakten Modellierungselemente und Bezüge für den softwaretechnischen Entwurf 
- beschreibt die zusammenarbeitenden Objekte und Klassen, die maßgeschneidert sind, um ein allgemeines Entwurfsproblem in einem bestimmten Kontext zu lösen 
- muss durch unterschiedliche Konstruktionsansätze konkretisiert werden. Ein Konstruktionsansatz als der konstruktive Teil eines Entwurfsmusters
- beschreibt die konkreten Elemente und Bezüge des softwaretechnischen Entwurfs 
- ist i.d.R. eine Implementationsvariante, die einen Aspekt des Musters betont und auf eine bestimmte Programmiersprache zugeschnitten ist 
- kann unmittelbar in eine programmiersprachliche Implementation umgesetzt werden
<br><br>

## Strategy Pattern

Das Strategy Pattern gehört zu den Behavioral Patterns (Verhaltensmustern), die eine Software mit verschiedenen Lösungsmethoden ausstatten. Hinter den Strategien steht eine Familie von Algorithmen, die vom eigentlichen Programm abgegrenzt werden und autonom (= austauschbar) sind. Zu einem Strategie-Entwurfsmuster gehören auch gewisse Vorgaben und Hilfestellungen für Entwickler. So beschreiben Strategy Patterns, wie man Klassen aufbaut, eine Gruppe von Klassen arrangiert und Objekte erstellt. Eine Besonderheit des Strategy Design Patterns ist, dass ein variables Programm- und Objektverhalten auch zur Laufzeit einer Software realisiert werden kann.
<br><br>

### UML - Strategy Pattern
<br>

![UML-Strategy-Pattern](images/strategy-pattern-uml.jpg)

*Grundaufbau eines Strategie-Entwurfsmusters in UML mit drei Grundkomponenten: Context (Hauptklasse), Strategy (Interface) und ConcreteStrategies (ausgelagerte Algorithmen und Lösungsvorschriften für die konkrete Problemlösung).*
<br><br>

Im Strategy Design Pattern übernehmen die Grundkomponenten spezielle Funktionen: Die Verhaltensmuster der Context-Klasse werden in verschiedene Strategy-Klassen ausgelagert. Diese separaten Klassen beherbergen die Algorithmen, die als ConcreteStrategies bezeichnet werden. Über eine Referenz (also einen internen Verweis) kann der Context bei Bedarf auf die ausgelagerten Berechnungsvarianten (ConcreteStrategyA, ConcreteStrategyB etc.) zugreifen. Dabei interagiert er nicht direkt mit den Algorithmen, sondern mit einer Schnittstelle.

Das Strategy-Interfacekapselt die Berechnungsvarianten und kann zugleich von allen Algorithmen implementiertwerden. Für die Interaktion mit dem Context stellt die generische Schnittstelle nur eine einzige Methode zum Auslösen von ConcreteStrategy-Algorithmen zur Verfügung. Zu den Interaktionen mit dem Context gehört neben dem Strategieaufruf auch der Austausch von Daten. An Strategiewechseln, die auch zur Laufzeit eines Programms stattfinden können, ist das Strategy-Interface ebenfalls beteiligt.
<br><br>

**Fakt**
Mit einer Kapselung wird der direkte Zugriff auf Algorithmen und interne Datenstrukturen unterbunden. Ein externe Instanz (Client, Context) kann Berechnungen und Funktionen ausschließlich über definierte Schnittstellen in Anspruch nehmen. Dabei sind nur diejenigen Methoden und Datenelemente eines Objekts zugänglich, die für die externe Instanz relevant sind.
<br><br>

# Entwurfsmuster 2

## Chain of Responsibility

Die Zuständigkeitskette (englisch chain of responsibility) ist ein in der Softwareentwicklung eingesetztes Entwurfsmuster. Es gehört zur Kategorie der Verhaltensmuster und wird für Algorithmen verwendet. Dabei dient es der Entkopplung des Auslösers einer Anfrage mit seinem Empfänger.
<br><br>

### Verwendung

Mehrere Objekte werden hintereinander geschaltet (miteinander verkettet), um gemeinsam eine eingehende Anfrage bearbeiten zu können. Diese Anfrage wird an der Kette entlang geleitet, bis eines der Objekte die Anfrage beantworten kann. Der Klient, von dem die Anfrage ausgeht, hat dabei keine Kenntnis darüber, von welchem Objekt die Anfrage beantwortet werden wird.
<br><br>

## Template Hook Pattern

Die Schablonenmethode (Template method pattern) ist ein in der Softwareentwicklung eingesetztes Entwurfsmuster, mit dem Teilschritte eines Algorithmus variabel gehalten werden können.
Beim Template Method-Entwurfsmuster wird in einer abstrakten Klasse das Skelett eines Algorithmus definiert. Die konkrete Ausformung der einzelnen Schritte wird an Unterklassen delegiert. Dadurch besteht die Möglichkeit, einzelne Schritte des Algorithmus zu verändern oder zu überschreiben, ohne dass die zu Grunde liegende Struktur des Algorithmus modifiziert werden muss. Die Template Method ruft abstrakte Methoden auf, die erst in den Unterklassen definiert werden. Diese Methoden werden auch als Einschubmethoden bezeichnet.

![Template-Method-Pattern](images/template_method_patternpng.png)

<br>

## Decorator Pattern


Das Decorator Design Pattern, kurz Decorator Pattern (dt. Decorator-Muster), ist eine Musterstrategie für die übersichtliche Erweiterung von Klassen in objektorientierter Computersoftware.
Nach dem Muster lässt sich jedes beliebige Objekt um ein gewünschtes Verhalten ergänzen, ohne dabei das Verhalten anderer Objekte derselben Klasse zu beeinflussen. Strukturell ähnelt das Decorator Pattern stark dem „Chain of responsibility“-Pattern (dt. Zuständigkeitskette), wobei Anfragen anders als bei diesem Zuständigkeitskonzept mit zentralem Bearbeiter von allen Klassen entgegengenommen werden.

Die Software-Komponente, die erweitert werden soll, wird nach dem Decorator-Entwurfsmuster mit einer bzw. mehreren Decorator-Klassen „dekoriert“, die die Komponente vollständig umschließen. Jeder Decorator ist dabei vom selben Typ wie die umschlossene Komponente und verfügt damit über die gleiche Schnittstelle. Dadurch kann er eingehende Methodenaufrufe unkompliziert an die verknüpfte Komponente delegieren, während er wahlweise zuvor bzw. anschließend das eigene Verhalten ausführt. Auch eine direkte Verarbeitung eines Aufrufs im Decorator ist grundsätzlich möglich.
<br><br>

### Welchen Zweck erfüllt das Decorator Design Pattern?

Wie andere GoF-Muster, etwa das Strategy Pattern oder das Builder Pattern, verfolgt das Decorator Pattern das Ziel, Komponenten objektorientierter Software flexibler und einfacher wiederverwendbar zu gestalten. Zu diesem Zweck liefert der Ansatz die Lösung dafür, Abhängigkeiten zu einem Objekt dynamisch und – insofern erforderlich – während der Laufzeit hinzufügen bzw. entfernen zu können. Insbesondere aus diesem Grund stellt das Pattern eine gute Alternative zum Einsatz von Subklassen dar: Diese können eine Klasse zwar ebenfalls auf vielfältige Weise ergänzen, lassen jedoch keinerlei Anpassungen während der Laufzeit zu.
<br><br>

**Hinweis**
Eine Software-Komponente lässt sich durch beliebig viele Decorator-Klassen erweitern. Für zugreifende Instanzen bleiben diese Erweiterungen dabei gänzlich unsichtbar, sodass diese gar nicht mitbekommen, dass der eigentlichen Klasse zusätzliche Klassen vorgeschaltet sind.

Der Decorator bzw. die Decorator-Klassen (ConcreteDecorator) verfügen über die gleiche Schnittstelle wie die zu dekorierende Software-Komponente (ConcreteKomponente) und sind vom gleichen Typ. Dieser Umstand ist wichtig für das Handling der Aufrufe, die entweder unverändert oder verändert weitergeleitet werden, falls der Decorator die Verarbeitung nicht selbst übernimmt. Im Decorator-Pattern-Konzept bezeichnet man diese elementare Schnittstelle, die im Prinzip eine abstrakte Superklasse ist, als „Komponente“.

Das Zusammenspiel von Basiskomponente und Decorator lässt sich am besten in einer grafischen Darstellung der Beziehungen in Form eines UML-Klassendiagramms verdeutlichen. In der nachfolgenden abstrakten Abbildung des Decorator Design Patterns haben wir daher die Modellierungssprache für objektorientierte Programmierung verwendet.

![Decorator-Design-Pattern](images/decorator-design-pattern.jpg)

*Die beispielhafte Darstellung des Decorator Patterns zeigt zwei verschiedene ConcreteDecorator-Klassen mit zusätzlichen Funktionen, wobei die Anzahl in diesem Fall exemplarisch gewählt wurde.*
<br><br>

### Die Vorteile und Nachteile des Decorator Patterns im Überblick

Das Decorator-Muster bei der Konzeptionierung einer Software zu berücksichtigen, zahlt sich gleich aus mehreren Gründen aus. Allen voran steht das hohe Maß an Flexibilität, das mit einer solchen Decorator-Struktur einhergeht: Sowohl zur Kompilierungs- als auch zur Laufzeit lassen sich Klassen gänzlich ohne Vererbung um neues Verhalten erweitern. Zu unübersichtlichen Vererbungshierarchien kommt es bei diesem Programmieransatz folglich nicht, was nebenbei auch die Lesbarkeit des Programmcodes deutlich verbessert.

Dadurch, dass die Funktionalität auf mehrere Decorator-Klassen aufgeteilt wird, lässt sich außerdem die Performance der Software steigern. So kann man gezielt jene Funktionen aufrufen und initiieren, die man gerade benötigt. Bei einer komplexen Basisklasse, die alle Funktionen permanent bereitstellt, hat man diese ressourcenoptimierte Möglichkeit nicht.

Die Entwicklung nach dem Decorator Pattern bringt jedoch nicht nur Vorteile mit sich: Mit der Einführung des Musters steigt automatisch auch die Komplexität der Software. Insbesondere die Decorator-Schnittstelle ist in der Regel sehr wortreich sowie mit vielen neuen Begrifflichkeiten verknüpft und damit alles andere als einsteigerfreundlich. Ein weiterer Nachteil besteht in der hohen Anzahl an Decorator-Objekten, für die eine eigene Systematisierung zu empfehlen ist, um nicht mit ähnlichen Übersichtsproblemen wie bei der Arbeit mit Subklassen konfrontiert zu werden. Die oft sehr langen Aufrufketten der dekorierten Objekte (also der erweiterten Software-Komponenten) erschweren zudem das Auffinden von Fehlern und damit den Debugging-Prozess im Allgemeinen.
<br><br>

<table>
    <tbody>
        <tr>
            <td>Vorteile</td>
            <td>Nachteile</td>
        </tr>
        <tr>
            <td>Hohes Maß an Flexibilität</td>
            <td>Hohe Komplexität der Software (insbesondere der Decorator-Schnittstelle)</td>
        </tr>
        <tr>
            <td>Funktionserweiterung von Klassen ohne Vererbung</td>
            <td>Einsteigerunfreundlich</td>
        </tr>
        <tr>
            <td>Gut lesbarer Programmcode</td>
            <td>Hohe Anzahl an Objekten</td>
        </tr>
        <tr>
            <td>Ressourcenoptimierte Funktionsaufrufe</td>
            <td>Erschwerter Debugging-Prozess</td>
        </tr>
    </tbody>
</table>
<br><br>

## Builder Pattern

Sie erleichtern den Entwicklern die Programmierung, da nicht jeder wiederholt auftretende Schritt erneut als Programmroutine erdacht werden muss, sondern auf eine bereits etablierte Lösung zurückgegriffen werden kann.
<br><br>

### Das Builder Pattern im Detail

Das Builder Pattern gehört zur Gruppe der Erzeugungsmuster aus den Design Patterns. Es verbessert sowohl die Sicherheit bei der Konstruktion als auch die Lesbarkeit des Programmcodes. Das Ziel des Erbauer-Entwurfsmusters ist, dass ein Objekt nicht mit den bekannten Konstruktoren erstellt wird, sondern mittels einer Hilfsklasse.

Im Builder Design Pattern werden vier Akteure unterschieden:

- Direktor: Dieser Akteur konstruiert das komplexe Objekt, indem er die Schnittstelle des Erbauers verwendet. Er kennt die Anforderungen an die Arbeitsreihenfolge des Erbauers. Beim Direktor wird die Konstruktion eines Objekts vom Kunden („Auftraggeber“) entkoppelt.
- Erbauer: Hält eine Schnittstelle zur Erzeugung der Bestandteile eines komplexen Objekts (bzw. Produkts) bereit.
Konkreter Erbauer: Dieser Akteur erzeugt die Teile des komplexen Objekts und definiert (und verwaltet) die Repräsentation des Objekts und hält eine Schnittstelle zur Ausgabe des Objekts vor.
- Produkt: Das Ergebnis der „Tätigkeit“ des Builder Patterns, also das zu konstruierende komplexe Objekt.
  
Beim Direktor geschieht der entscheidende Vorgang des Erbauer-Musters, die Trennung der Herstellung eines Objekts/Produkts vom Auftraggeber.

Die Grafik zeigt, dass das Builder-Pattern aus mehreren, miteinander agierenden Objekten besteht.

![Builder-Pattern-UML](images/builder-pattern-uml-diagramm.jpg)
*Grundaufbau des Erbauer-Entwurfsmusters in UML, das die vielfältigen Beziehungen illustriert. Der eigentliche Nutzer (Kunde) des erzeugten Objekts ist von dessen Herstellung völlig abgekoppelt.*
<br><br>

### Vorteile des Builder-Patterns

Die Konstruktion (das Erbauen) und die Repräsentation (die Ausgabe) werden voneinander isoliert eingebunden. Die internen Repräsentationen der Erbauer sind vor dem Direktor „versteckt“. Neue Repräsentationen können leicht durch neue konkrete Erbauerklassen eingefügt werden. Der Prozess der Konstruktion wird vom Direktor als explizite Stelle gesteuert. Müssen dabei Änderungen vorgenommen werden, ist das ohne Rückfragen beim Klienten möglich.
<br><br>

### Nachteile des Builder Patterns

Es besteht eine enge Kopplung zwischen Produkt, konkretem Erbauer und den am Konstruktionsprozess beteiligten Klassen, so dass Änderungenam grundsätzlichen Prozessschwierig sein können. Das Erbauen (Konstruktion) der Objekte erfordert oft Wissen über deren spezielle Verwendung und ihre Umgebung. Die Nutzung von bekannten Patterns, so auch des Erbauer-Entwurfsmusters, können Programmierer dazu verleiten, einfachere und vielleicht elegantere Lösungen zu übersehen. Letztlich gilt das Erbauer-Muster unter Programmierern als eines der weniger wichtigen Entwurfsmuster.
<br><br>

### Wo kommt das Builder Pattern zum Einsatz?

Eine Möglichkeit, das Builder Patterns zu verdeutlichen, ist ein vereinfacht dargestelltes Restaurant, in dem ein Gast ein Menü bestellt. Die verschiedenen Akteure des Restaurants agieren nach dem Eingang der Bestellung, um diese auszuführen. Der gesamte Vorgang bis zum Erhalt des bestellten Menüs spielt sich „hinter den Kulissen“ ab. Der Gast sieht z. B. nicht, was in der Küche geschieht, um seine Bestellung auszuführen – er bekommt das Ergebnis an seinem Tisch serviert.
<br><br>

![Builder-Pattern-Bsp](images/builder-pattern-beispiel.jpg)
*Die Bestellung eines Gastes im Restaurant ist von der Produktion seines Menüs getrennt. Der Direktor und die Builder sorgen für die Herstellung des Produkts mittels des Builder Patterns.*
<br><br>

## Adapter

Der Adapter (englisch adapter pattern) – auch die Hüllenklasse oder der Wrapper genannt – ist ein Entwurfsmuster aus dem Bereich der Softwareentwicklung, das zur Kategorie der Strukturmuster (engl. structural patterns) gehört. Das Muster dient zur Übersetzung einer Schnittstelle in eine andere. Dadurch wird die Kommunikation von Klassen mit zueinander inkompatiblen Schnittstellen ermöglicht.
<br><br>

### Verwendung

Der Adapter findet Anwendung, wenn eine existierende Klasse verwendet werden soll, deren Schnittstelle nicht der benötigten Schnittstelle entspricht. Dies tritt insbesondere dann auf, wenn Klassen, die zur Wiederverwendung konzipiert wurden – wie Werkzeugsammlungen oder Klassenbibliotheken – verwendet werden sollen. Diese stellen ihre Dienste durch klar definierte Schnittstellen zur Verfügung, die in der Regel nicht geändert werden sollen und häufig auch nicht geändert werden können, da sie von Dritten stammen. Des Weiteren wird der Adapter bei der Erstellung wiederverwendbarer Klassen benutzt, wenn diese mit unabhängigen oder nichtvorhersehbaren Klassen zusammenarbeiten sollen.
<br><br>

### UML Diagramm

Die sogenannte „Gang of Four“ (Viererbande) beschreibt zwei Realisierungsalternativen. Die erste ist der Adapter mit Delegation (der sogenannte Objektadapter), die zweite der Adapter mit Vererbung (Klassenadapter).

#### Adapter mit Delegation (Objektadapter)

Hierbei hat der Adapter eine Assoziation zu der zu adaptierenden Klasse und leitet die Anfragen per Delegation weiter.

Der Vorteil ist, dass der Adapter und der dahinterliegende Dienst ausgetauscht werden können; dafür muss die gesamte genutzte Schnittstelle implementiert werden, auch wenn nur ein Teil der Implementierung angepasst werden soll.

![Objektadapter](images/Objektadapter.png)
*Objektadapter sind auch als Hüllenklasse oder Wrapper-Klasse bekannt. Dabei können nicht nur andere Klassen gekapselt werden, sondern auch primitive Datentypen oder prozedurale Programmierbibliotheken.*
<br><br>

### Adapter mit Vererbung (Klassenadapter)

Ein Klassenadapter wird mit Hilfe von Mehrfachvererbung realisiert. Zum einen erbt er die Implementierung der zu adaptierenden Klasse. Zum anderen die zu implementierende Schnittstelle. Der Aufruf erfolgt dann durch Selbstdelegation.

![Klassenadapter](images/Klassenadapter.png)

### Vor- und Nachteile

Die Vorteile eines Klassenadapters bestehen darin, dass er sich genau einer Zielklasse anpasst und dadurch nur das Verhalten der Zielklasse überschreiben kann. Der Objektadapter kann auch Unterklassen mit anpassen.

Nachteilig wirkt sich aus, dass ein Klassenadapter nicht zur automatischen Anpassung von Unterklassen verwendet werden kann.
<br><br>

## Observer Pattern

Das Observer Design Pattern, kurz Observer Pattern bzw. deutsch Beobachter-Entwurfsmuster, ist eine der beliebtesten Mustervorlagen für das Design von Computersoftware. Es gibt eine einheitliche Möglichkeit an die Hand, eine Eins-zu-eins-Abhängigkeit zwischen zwei oder mehreren Objekten zu definieren, um sämtliche Änderungen an einem bestimmten Objekt auf möglichst unkomplizierte und schnelle Weise zu übermitteln. Zu diesem Zweck können sich beliebige Objekte, die in diesem Fall als Observer bzw. Beobachter fungieren, bei einem anderen Objekt registrieren. Letzteres Objekt, das man in diesem Fall auch als Subjekt bezeichnet, informiert die registrierten Beobachter, sobald es sich verändert bzw. angepasst wird.

Das Observer Pattern zählt, wie eingangs bereits erwähnt, zu den 1994 in „Design Patterns: Elements of Reusable Object-Oriented Software“ veröffentlichten GoF-Mustern. Die über 20 beschriebenen Musterlösungen für das Softwaredesign spielen bis heute eine wichtige Rolle in der Konzeptionierung und Ausarbeitung von Computeranwendungen.
<br><br>

### Zweck und Funktionsweise des Observer Patterns

Das Beobachter-Entwurfsmuster arbeitet mit zwei Typen von Akteuren: Auf der einen Seite steht das Subjekt, also das Objekt, dessen Status langfristig unter Beobachtung stehen soll. Auf der anderen Seite stehen die beobachtenden Objekte (Observer bzw. Beobachter), die über sämtliche Änderungen des Subjekts in Kenntnis gesetzt werden wollen.
<br><br>

**Fakt**
Typischerweise werden einem Subjekt gleich mehrere Observer zugeordnet. Prinzipiell kann das Observer Pattern aber auch nur für ein einziges beobachtendes Objekt angewendet werden.

Ohne den Einsatz des Beobachter-Patterns müssten die beobachtenden Objekte das Subjekt in regelmäßigen Intervallen um Status-Updates bitten – jede einzelne Anfrage wäre mit entsprechender Rechenzeit inklusive der hierfür erforderlichen Hardware-Ressourcen verbunden. Die grundlegende Idee des Observer Patterns ist es, die Aufgabe des Informierens im Subjekt zu zentralisieren. Zu diesem Zweck führt es eine Liste, in die sich die Beobachter eintragen können. Bei einer Änderung informiert das Subjekt die registrierten Observer der Reihe nach, ohne dass diese selbst aktiv werden müssen. Ist ein automatisches Status-Update nicht mehr für ein bestimmtes, beobachtendes Objekt gewünscht, trägt man es einfach wieder aus der Liste aus.
<br><br>

**Hinweis**
Zur Information der einzelnen Beobachter sind zwei verschiedene Methoden möglich: Bei der Push-Methode übermittelt das Subjekt den geänderten Zustand bereits mit der Benachrichtigung. Das kann jedoch zu Problemen führen, wenn Informationen übermittelt werden, die der Observer nicht verwerten kann. Dieses Problem besteht bei der alternativen Pull-Methode nicht: Bei diesem Ansatz gibt das Subjekt lediglich die Information weiter, dass Änderungen vorgenommen wurden. Die Beobachter müssen den geänderten Zustand anschließend per gesondertem Methodenaufruf erfragen.
<br><br>

### Grafische Darstellung des Observer Patterns (UML-Diagramm)

![Observer-Pattern](images/observer-pattern-uml.jpg)
*Im Beispiel ist nur ein einziger Beobachter aufgeführt – in der Praxis kann jedoch eine Vielzahl an Observern existieren und den aktuellen Status des Subjekts über ConcreteObserver und das ConcreteSubjekt erfragen und aktualisieren.*
<br><br>

### Welche Vor- und Nachteile hat das Observer Design Pattern?

Das Observer Pattern in der Software-Entwicklung einzusetzen, kann sich in vielen Situationen auszahlen. Der große Vorteil, den das Konzept bietet, ist dabei der hohe Unabhängigkeitsgrad zwischen einem beobachteten Objekt (Subjekt) und den beobachtenden Objekten, die sich an dem aktuellen Zustand dieses Objekts orientieren. Das beobachtete Objekt muss beispielsweise keinerlei Informationen über seine Beobachter besitzen, da die Interaktion unabhängig über die Observer-Schnittstelle erledigt wird. Die beobachtenden Objekte erhalten die Updates derweil automatisch, wodurch ergebnislose Anfragen im Observer-Pattern-System (weil sich das Subjekt nicht geändert hat) gänzlich wegfallen.

Dass das Subjekt alle registrierten Beobachter automatisch über jegliche Änderungen informiert, ist jedoch nicht immer von Vorteil: Die Änderungsinformationen werden nämlich auch dann übermittelt, wenn sie für einen der Observer irrelevant sein sollten. Das wirkt sich insbesondere dann negativ aus, wenn die Zahl an registrierten Beobachtern sehr groß ist, da an dieser Stelle durch das Observer-Schema eine Menge Rechenzeit verschenkt werden kann. Ein weiteres Problem des Beobachter-Entwurfsmusters: Häufig ist im Quellcode des Subjekts nicht ersichtlich, welche Beobachter mit Informationen versorgt werden.
<br><br>

### Wo wird es eingesetzt?

Das Observer Design Pattern ist insbesondere in Anwendungen gefragt, die auf Komponenten basieren, deren Status

einerseits stark von anderen Komponenten beobachtet wird,
andererseits regelmäßigen Änderungen unterliegt.
<br><br>

# Solid Design Prinzipien

Bei den SOLID-Prinzipien handelt es sich um Designvorgaben, die in der Softwareentwicklung zum Einsatz kommen. Der Begriff beschreibt fünf Design-Prinzipien, die vorgeben wie Funktionen und Datenstrukturen in Klassen angeordnet sind und wie diese Klassen miteinander verbunden sein sollen.

Ziel von SOLID ist die Erzeugung von Software, die Modifikationen toleriert, leicht nachvollziehbar ist und die eine Basis der Komponenten bildet, die in vielen Softwaresystemen eingesetzt werden.

Im Folgenden soll ein möglichst verständlicher Überblick der Prinzipien gegeben werden. Dabei werde ich nicht zu sehr ins Detail gehen, da weitaus umfangreichere Publikationen und Artikel zu den einzelnen Prinzipien existieren.

- **S** Single Responsibility Prinzip
- **O** Open Closed Prinzip
- **L** Liskov'sche Substitutions Prinzip
- **I** Interface Segegation Prinzip
- **D** Dependency Inversion Prinzip

## Single Responsibility Prinzip

Das Single-Responsibility-Prinzip besagt, dass jede Klasse nur eine einzige Verantwortung haben sollte. Verantwortung wird hierbei als „Grund zur Änderung“ definiert: „Es sollte nie mehr als einen Grund dafür geben, eine Klasse zu ändern.“

Mehr als eine Verantwortung für eine Klasse führt zu mehreren Bereichen, in denen zukünftige Änderungen notwendig werden können. Die Wahrscheinlichkeit, dass die Klasse zu einem späteren Zeitpunkt geändert werden muss, steigt zusammen mit dem Risiko, sich bei solchen Änderungen subtile Fehler einzuhandeln. Dieses Prinzip führt in der Regel zu Klassen mit hoher Kohäsion, in denen alle Methoden einen starken gemeinsamen Bezug haben.

## Open Closed Prinzip

Das Open-Closed-Prinzip besagt, dass Software-Einheiten ( Module, Klassen, Methoden usw.) Erweiterungen möglich machen sollen (dafür offen sein), aber ohne dabei ihr Verhalten zu ändern (ihr Sourcecode und ihre Schnittstelle sollte sich nicht ändern). Es wurde folgendermaßen formuliert: „Module sollten sowohl offen (für Erweiterungen), als auch geschlossen (für Modifikationen) sein.“

Eine Erweiterung im Sinne des Open-Closed-Prinzips ist beispielsweise die Vererbung. Diese verändert das vorhandene Verhalten einer Klasse nicht, erweitert sie aber um zusätzliche Funktionen oder Daten. Überschriebene Methoden verändern auch nicht das Verhalten der Basisklasse, sondern nur das der abgeleiteten Klasse. Folgt man weiter dem Liskovschen Substitutionsprinzip, verändern auch überschriebene Methoden nicht das Verhalten, sondern nur die Algorithmen.

## Liskovsches Substitutions Prinzip

Das Liskovsche Substitutionsprinzip (LSP) oder Ersetzbarkeitsprinzip fordert, dass eine Instanz einer abgeleiteten Klasse sich so verhalten muss, dass jemand, der meint, ein Objekt der Basisklasse vor sich zu haben, nicht durch unerwartetes Verhalten überrascht wird, wenn es sich dabei tatsächlich um ein Objekt eines Subtyps handelt.

Damit ist garantiert, dass Operationen vom Typ Superklasse, die auf ein Objekt des Typs Subklasse angewendet werden, auch korrekt ausgeführt werden. Dann lässt sich stets bedenkenlos ein Objekt vom Typ Superklasse durch ein Objekt vom Typ Subklasse ersetzen. Objektorientierte Programmiersprachen können eine Verletzung dieses Prinzips, die aufgrund der mit der Vererbung verbundenen Polymorphie auftreten kann, nicht von vornherein ausschließen. Häufig ist eine Verletzung des Prinzips nicht auf den ersten Blick offensichtlich.

## Interface Segregation Prinzip

Das Interface-Segregation-Prinzip dient dazu, zu große Interfaces aufzuteilen. Die Aufteilung soll gemäß den Anforderungen der Clients an die Interfaces gemacht werden – und zwar derart, dass die neuen Interfaces genau auf die Anforderungen der einzelnen Clients passen. Die Clients müssen also nur mit Interfaces agieren, die das und nur das können, was die Clients benötigen. Das Prinzip wurde folgendermaßen formuliert: „Clients sollten nicht dazu gezwungen werden, von Interfaces abzuhängen, die sie nicht verwenden.“

Mit Hilfe des Interface-Segregation-Prinzips ist es möglich eine Software derart in entkoppelte und somit leichter refaktorisierbare Klassen aufzuteilen, dass zukünftige fachliche oder technische Anforderungen an die Software nur geringe Änderungen an der Software selbst benötigen.

## Dependency Inversion Prinzip

Das Dependency-Inversion-Prinzip beschäftigt sich mit der Reduktion der Kopplung von Modulen. Es besagt, dass Abhängigkeiten immer von konkreteren Modulen niedriger Ebenen zu abstrakten Modulen höherer Ebenen gerichtet sein sollten.

A. Module hoher Ebenen sollten nicht von Modulen niedriger Ebenen abhängen. Beide sollten von Abstraktionen abhängen.
B. Abstraktionen sollten nicht von Details abhängen. Details sollten von Abstraktionen abhängen.

Damit ist sichergestellt, dass die Abhängigkeitsbeziehungen immer in eine Richtung verlaufen, von den konkreten zu den abstrakten Modulen, von den abgeleiteten Klassen zu den Basisklassen. Damit werden die Abhängigkeiten zwischen den Modulen reduziert und insbesondere zyklische Abhängigkeiten vermieden.
<br><br>

# Weitere Prinzipien des objektorientierten Entwurfs

## KISS Prinzip

Das KISS-Prinzip (Keep it simple, stupid) fordert, zu einem Problem eine möglichst einfache Lösung anzustreben.

In seiner Grundaussage ähnelt das KISS-Prinzip stark der Aussage: "Wenn es mehrere Erklärungen für einen bestimmten Sachverhalt gibt, dann ist diejenige Erklärung zu bevorzugen, die am einfachsten ist, also mit den wenigsten Annahmen und Variablen auskommt." Es handelt sich hierbei auch um ein Prinzip von Clean Code.

Als Designprinzip beschreibt es im Gegensatz zu einer Problemlösung in der Form einer Fehlerumgehung („workaround“) die möglichst einfache, minimalistische und leicht verständliche Lösung eines Problems.

## DRY Prinzip

Don’t repeat yourself (DRY für „wiederhole dich nicht“; auch bekannt als once and only once „einmal und nur einmal“) ist ein Prinzip, das besagt, Redundanz zu vermeiden oder zumindest zu reduzieren. Es handelt sich hierbei auch um ein Prinzip von Clean Code.

Redundant vorhandene Informationen (zum Beispiel Code-Duplikate im Quelltext) sind nur aufwändig einheitlich zu pflegen. Bei Systemen, die dem DRY-Prinzip treu bleiben, brauchen Änderungen nur an einer Stelle vorgenommen zu werden.

## YAGNI Prinzip

YAGNI steht für You Aren’t Gonna Need It, „Du wirst es nicht brauchen“. Es bezeichnet ein Prinzip des Extreme Programming (XP), das besagt, dass in einem Programm erst dann Funktionalität implementiert werden sollte, wenn klar ist, dass diese Funktionalität tatsächlich gebraucht wird.

Entgegen diesem Vorgehen wird in der Praxis oft versucht, Programme durch zusätzlichen oder allgemeineren (generischen) Code auf mögliche künftige Änderungsanforderungen vorzubereiten. Die dahinter liegende Überlegung ist, dass Änderungen später aufwändiger umzusetzen sind als sie jetzt bereits vorwegzunehmen.

Oft stellt sich später heraus, dass dieser Zusatzaufwand unnötig war, weil sich die ursprünglich erwartete Anforderung in der gedachten Form tatsächlich nie ergeben hat. Stattdessen ergeben sich häufig Anforderungen, die bei der ursprünglichen Entwicklung nicht vorhergesehen wurden und deren Umsetzung durch den Code, der eigentlich Änderungen erleichtern sollte, nicht unterstützt und oft sogar behindert wird.

YAGNI führt also in erster Linie zu schlankerem, einfacherem Code und ermöglicht damit, später kommende Anforderungen kostengünstig umzusetzen.

Weitere Überlegungen hinter YAGNI sind:

- Zielgerichtete Verwendung der Arbeitszeit für die Umsetzung der aktuell geforderten Funktionalität führt dazu, dass diese rascher und oft besser umgesetzt wird. Dies steigert die Zufriedenheit der Anforderer mehr als die Umsetzung noch nicht geforderter Funktionalitäten.
  
- Die Umsetzung noch nicht geforderter Funktionalitäten kann dazu führen, dass weitere unangeforderte Funktionalitäten den Umsetzern als sinnvoll erscheinen und ebenfalls umgesetzt werden. Dies führt meist dazu, dass unerwünschte oder inkorrekte Funktionalitäten umgesetzt werden.

YAGNI ist eines der Prinzipien hinter der XP-Technik von „Implementiere die einfachst mögliche Lösung, die funktioniert“.