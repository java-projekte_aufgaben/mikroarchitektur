package at.itkolleg;

import at.itkolleg.exceptions.KeineZustaendigkeit;

public interface Sammelumrechnung {

    public double sammelumrechnen(double [] betraege, String variante) throws KeineZustaendigkeit;
}
