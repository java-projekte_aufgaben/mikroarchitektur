//package at.itkolleg.builder;
//
//import at.itkolleg.chainOfResponsibility.WR;
//import at.itkolleg.decorator.HeaderDekorierer;
//
//public class WRBuilder {
//
//    private WR aktuell;
//
//    public WRBuilder setChainBegin(WR aktuell,WR next) {
//        this.aktuell = aktuell;
//        aktuell.setNext(next);
//        return this;
//    }
//    public WRBuilder setNextChainMember(WR next,WR nextNext){
//        next.setNext(nextNext);
//        return this;
//    }
//    public WRBuilder setChainwithDeco(WR aktuell,WR next){
//        this.aktuell=new HeaderDekorierer(aktuell);
//        aktuell.setNext(next);
//        return this;
//    }
//    public WR build(){
//        return aktuell;
//    }
//}
