package at.itkolleg;

import at.itkolleg.exceptions.KeineZustaendigkeit;

public interface Umrechnen {

    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeit;
}
