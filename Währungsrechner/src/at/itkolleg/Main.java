package at.itkolleg;

import at.itkolleg.adapter.AdapterSammelWR;
//import at.itkolleg.builder.WRBuilder;
import at.itkolleg.chainOfResponsibility.EURO2DOLLAR;
import at.itkolleg.chainOfResponsibility.EURO2YEN;
import at.itkolleg.decorator.HeaderDekorierer;
import at.itkolleg.exceptions.KeineZustaendigkeit;

public class Main {

    public static void main(String[] args) {

    //Chain of Responsibility
    Umrechnen kette = new EURO2DOLLAR(new EURO2YEN(null));

    //Decorator
    kette = new HeaderDekorierer(kette);

    //Sammelumrechnung
    double[] werte = {10, 50.5, 100};
    Sammelumrechnung sammelkette = new AdapterSammelWR(kette);

    //Builder
    //Umrechnen builder = new WRBuilder().setChainBegin().setNextChainMember();


    try{
        System.out.println("\n++++++++++ Währungsrechner ++++++++++");
        System.out.println(kette.umrechnen("EURO2DOLLAR", 100.00));
        System.out.println(kette.umrechnen("EURO2YEN", 100.00));

        System.out.println("\n++++++++++ Sammelumrechnung ++++++++++");
        System.out.println(sammelkette.sammelumrechnen(werte, "EURO2DOLLAR"));
        System.out.println(sammelkette.sammelumrechnen(werte, "EURO2YEN"));

    } catch(KeineZustaendigkeit keineZustaendigkeit) {
        System.out.println(keineZustaendigkeit.getMessage());
        }


    }
}
