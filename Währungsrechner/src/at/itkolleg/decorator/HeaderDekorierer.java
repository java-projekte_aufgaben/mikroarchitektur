package at.itkolleg.decorator;

import at.itkolleg.Umrechnen;
import at.itkolleg.exceptions.KeineZustaendigkeit;

public class HeaderDekorierer extends Dekorierer{


    public HeaderDekorierer(Umrechnen naechsterDekorierer) {

        super(naechsterDekorierer);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeit {

        if(variante.equals("EURO2DOLLAR")){
            System.out.println("EUR zu DOLLAR");
            return super.umrechnen(variante,betrag * 1.05);
        }else if (variante.equals("EURO2YEN")){
            System.out.printf("EUR zu YEN\n");
            return super.umrechnen(variante, betrag + 5);
        }
        return super.umrechnen(variante, betrag);
    }


}
