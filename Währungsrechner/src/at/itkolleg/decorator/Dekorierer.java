package at.itkolleg.decorator;

import at.itkolleg.Umrechnen;
import at.itkolleg.chainOfResponsibility.WR;
import at.itkolleg.exceptions.KeineZustaendigkeit;

public abstract class Dekorierer implements Umrechnen {

    private Umrechnen naechsterWR;

    public Dekorierer(Umrechnen naechsterWR) {

        this.naechsterWR = naechsterWR;
    }

    @Override
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeit {
        if (this.naechsterWR != null){
            return naechsterWR.umrechnen(variante,betrag);
        }else {
            throw new KeineZustaendigkeit();
        }
    }
}
