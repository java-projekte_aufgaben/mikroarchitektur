package at.itkolleg.exceptions;

public class KeineZustaendigkeit extends Exception{

    public String message = "Keine Zuständigkeit gefunden!";

    public KeineZustaendigkeit() {

    }

    @Override
    public String getMessage() {
        return message;
    }
}
