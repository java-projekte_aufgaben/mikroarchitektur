package at.itkolleg.chainOfResponsibility;

import at.itkolleg.exceptions.KeineZustaendigkeit;

public class EURO2DOLLAR extends WR{

    private double exchangeRate = 1.13;

    public EURO2DOLLAR(WR naechsterWR) {

        super(naechsterWR);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeit {

        if(variante.equals("EURO2DOLLAR")){
            //Template Hook
            return exchange(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }
    }


    public double exchange(double betrag){

        return Math.round(betrag * getExchangeRate());
    }

    public double getExchangeRate() {

        return exchangeRate;
    }


}
