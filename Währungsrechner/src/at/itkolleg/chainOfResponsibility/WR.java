package at.itkolleg.chainOfResponsibility;

import at.itkolleg.Umrechnen;
import at.itkolleg.exceptions.KeineZustaendigkeit;

public abstract class WR implements Umrechnen {

    private Umrechnen naechsterWR;

    public WR(Umrechnen naechsterWR) {

        this.naechsterWR = naechsterWR;
    }

    @Override
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeit {

        if(this.naechsterWR != null){
            return naechsterWR.umrechnen(variante,betrag);
        } else {
            throw new KeineZustaendigkeit();
        }
    }

    public double exchange(double betrag){
        return Math.round(betrag * getExchangeRate());
    }


    //Template Hook
    abstract double getExchangeRate();

    // Builder
    //    public abstract void setNext(WR next);
}
