package at.itkolleg.chainOfResponsibility;

import at.itkolleg.Umrechnen;
import at.itkolleg.exceptions.KeineZustaendigkeit;

public class EURO2YEN extends WR{

    private double exchangeRate = 128.64;

    public EURO2YEN(WR naechsterWR) {

        super(naechsterWR);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeit {

        if(variante.equals("EURO2YEN")){
            //Template Hook
            return exchange(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }
    }

    public double exchange(double betrag){

        return Math.round(betrag * getExchangeRate());
    }


    public double getExchangeRate() {

        return exchangeRate;
    }


}
