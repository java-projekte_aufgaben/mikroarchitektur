package at.itkolleg.adapter;

import at.itkolleg.Sammelumrechnung;
import at.itkolleg.Umrechnen;
import at.itkolleg.exceptions.KeineZustaendigkeit;

public class AdapterSammelWR implements Sammelumrechnung {

    private Umrechnen naechsterWR;

    public AdapterSammelWR(Umrechnen naechsterWR){

        this.naechsterWR = naechsterWR;
    }

    @Override
    public double sammelumrechnen(double[] betraege, String variante) throws KeineZustaendigkeit {
        double gesamtbetrag = 0;
        double ergebnis = 0;
        for (double betrag : betraege){
            ergebnis = naechsterWR.umrechnen(variante, betrag);
            gesamtbetrag += betrag;
            System.out.println(ergebnis);
        }
        System.out.println("Gesamt gewechselte Euro");
        return gesamtbetrag;
    }
}
