package at.itkolleg;

interface Bird
{
    // birds implement Bird interface that allows
    // them to fly and make sounds, if implements Adapter Interface

    public void fly();
    public void makeSound();
}
