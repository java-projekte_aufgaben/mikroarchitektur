package at.itkolleg;

public class Main {

    public static void main(String[] args) {

        Sparrow sparrow = new Sparrow();
        ToyDuck plasticSparrow = new PlasticSparrow();

        // Wrap a bird in a birdAdapter so that it
        // behaves like toy duck
        ToyDuck birdAdapter = new BirdAdapter(sparrow);

        System.out.println("Sparrow...");
        sparrow.fly();
        sparrow.makeSound();

        System.out.println("--------------------");

        System.out.println("ToyDuck...");
        plasticSparrow.squeak();

        System.out.println("--------------------");

        // toy duck behaving like a bird
        System.out.println("BirdAdapter...");
        birdAdapter.squeak();
    }
}
