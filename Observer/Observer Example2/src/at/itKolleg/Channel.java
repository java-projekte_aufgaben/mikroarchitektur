package at.itKolleg;

public interface Channel {
    public void update(Object o);
}
