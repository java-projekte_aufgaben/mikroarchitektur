package at.itKolleg;

import java.nio.channels.Channel;
import java.util.ArrayList;
import java.util.List;

/**
 * Observer ist ein verhaltensorientiertes Entwurfsmuster. Es spezifiziert die Kommunikation zwischen Objekten: Observable und Observer.
 * Ein Observable ist ein Objekt, das Beobachter über Änderungen seines Zustands benachrichtigt.
 * Zum Beispiel kann eine Nachrichtenagentur Kanäle benachrichtigen, wenn sie Nachrichten erhält.
 * Durch den Empfang von Nachrichten ändert sich der Zustand der Nachrichtenagentur, und die Kanäle werden benachrichtigt.
 *
 * NewsAgency ist ein Observable, und wenn Nachrichten aktualisiert werden, ändert sich der Zustand von NewsAgency.
 * Wenn die Änderung eintritt, benachrichtigt NewsAgency die Beobachter über diese Tatsache, indem es deren update()-Methode aufruft.
 * Um das tun zu können, muss das Observable-Objekt Referenzen zu den Beobachtern behalten, und in unserem Fall ist das die Variable channels.
 * Schauen wir uns nun an, wie der Beobachter, die Klasse Channel, aussehen kann. Sie sollte die Methode update() haben, die aufgerufen wird, wenn sich der Zustand von NewsAgency ändert
 *
 */
public class NewsAgency {

    private String news;
    private List<Channel> channels = new ArrayList<>();

    public void addObserver(Channel channel){
        this.channels.add(channel);
    }

    public void removeObserver(Channel channel){
        this.channels.remove(channel);
    }

    public void setNews(String news){
        this.news = news;
        for(Channel channel : this.channels){
            channel.update(this.news);
        }
    }
}
